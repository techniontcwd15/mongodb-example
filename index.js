const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');

const url = 'mongodb://localhost:27017';

let db;


const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  if(!db)
    return res.sendStatus(331);

  next();
})

app.get('/test', async (req, res) => {
  const collection = db.collection('someCollection');

  const data = await collection.find({}).toArray();

  res.send(data);
});

app.post('/test', async (req, res) => {
  const body = req.body;
  const collection = db.collection('someCollection');

  await collection.insertOne(body);

  res.sendStatus(201);
});


app.listen(8000, async () => {
  const client = await MongoClient.connect(url);
  db = client.db('test');

  console.log('server is up');
});
